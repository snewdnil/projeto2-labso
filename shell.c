/*-------------------------------------------------------
 * Laboratorio de Sistemas Operacionais
 * Projeto 2: Gerencia de Processos
 * Nome: Henrique Teruo Eihara RA: 490016
 * Nome: Marcello da Costa Marques Acar RA: 552550
 -------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define MAX 256

// função que recebe uma string e quebra ela
// em cadeias de string, sendo a última cadeia
// apontada para NULL
char ** parser(char * comando){
  int tamanho = strlen(comando);
  int i;
  char ** aux;
  int tamanhoParam = 0;
  int flag = 1;

  if(comando == NULL)
    return NULL;

  for(i=0;i<tamanho && (comando[i] != '>') && (comando[i] != '<');i++){
    if(comando[i] != ' ' && flag){
      aux = (char**)realloc(aux,(1+tamanhoParam)*sizeof(char*));
      aux[tamanhoParam++] = &comando[i];
      flag = 0;
    }else{
      if(comando[i] == ' '){
        flag = 1;
        comando[i] = '\0';
      }
    }
  }
  aux = (char**)realloc(aux,sizeof(char*)*(++tamanhoParam));
  aux[tamanhoParam-1] = NULL;

  return aux;
}

int main() {
  char comando[MAX];
  char comandoAux[MAX];
  char comandoAux2[MAX];
  char ** aux = NULL;

  int i,i2;
  int pid;
  int acumulador = 0;
  int flag = 0;
  int paramMaior = 0;
  int paramMenor = 0;
  char  * variosProgramas = NULL;

  // loop principal
  while (1) {

    acumulador = 0;
    flag = 0;
    paramMaior = 0;
    paramMenor = 0;

    printf("> ");
    fgets(comando,255,stdin);
    strcpy(comandoAux, comando);

    // quando encontra na cadeia de strings & e \n,
    // ele é trocado automaticamente por um \0
    // ex: ronaldo & joga muito &\n
    // ronaldo \0 joga muito \0\0
    variosProgramas = strtok(comandoAux,"&\n");

    // segundo loop, que para quando encontrar quanodo
    // não há mais tokens, no caso \0
    while(variosProgramas != NULL){

      // é copiado a string com os tokens em comandoAux
      strcpy(comandoAux2, variosProgramas);
      strcpy(comandoAux, variosProgramas);

      // função que quebra a string em várias cadeias
      // e retorna um vetor de ponteiros de char
      aux = parser(comandoAux2);
      // printado na tela qual comando que ira ser executado
      // printf("executando %s ||||\n",comandoAux); // aqui seria  ahora da execucao

      // caso o comando seja exit, saiará do terminal
      if(!strcmp(aux[0], "exit")){
        exit(EXIT_SUCCESS);
      }

      // loop que verifica quando se há operadores > ou <

      i2 = strlen(comandoAux);

      for(i=0;i < i2;i++){
        if(comandoAux[i] == '>'){
          while(comandoAux[i+1] == ' ')
            i++;
          paramMaior = i;
          while(comandoAux[i+1] != ' ')
            i++;
          comandoAux[i+1] = '\0';
        }else{
          if(comandoAux[i] == '<'){
            while(comandoAux[i+1] == ' ')
              i++;
            paramMenor = i;
            while(comandoAux[i+1] != ' ')
              i++;
            comandoAux[i+1] = '\0';
          }
        }
      }

      // acumulador para colcoar /0 no & existente em
      // comandoAux
      acumulador += i2;

      if(comando[acumulador] == '&'){
        flag = 1;
      }else{
        flag = 0;
      }

      pid = fork();

      // caso exista &
      if(flag){
        if (pid) {
          // o terminal não ficará em espera...
        } else {
          if(paramMaior)
            freopen(comandoAux+paramMaior+1,"w+",stdout);

          if(paramMenor)
            freopen(comandoAux+paramMenor+1,"r",stdin);

          execvp(aux[0], aux);
          printf("Erro ao executar comando!\n");
          exit(EXIT_FAILURE);
        }
      //caso nao exista o &, o programa irá aguardar até que
      //a instância aberta seja fechada
      }else{
        if (pid) {
          waitpid(pid, NULL, 0);
        } else {
          if(paramMaior){
            freopen(comandoAux+paramMaior+1,"w+",stdout);
          }

          if(paramMenor){
            freopen(comandoAux+paramMenor+1,"r",stdin);
          }

          execvp(aux[0], aux);
          printf("Erro ao executar comando!\n");
          exit(EXIT_FAILURE);
        }
      }

      free(aux);

      // strtok nessa caso entregará o ponteiro +1
      // aonde esta o caracter &.
      // ex: ronaldo & joga muito
      // ponteiro ---> joga muito
      variosProgramas = strtok(NULL,"&\n");
    }
  }
}
